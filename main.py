import argparse
import imp
import os

from lag.generate import generate_track_midi, generate_track_click_midi


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate Ableton Live Track for arrangement view')
    parser.add_argument('input_file', help='Song config file path', type=str)
    parser.add_argument('out_file', help='Result file path', type=str)

    args = parser.parse_args()

    input_file = os.path.join(os.getcwd(), args.input_file)
    if not os.path.exists(input_file):
        raise ValueError('Incorrect file path')

    module = imp.load_source('song', input_file)

    click_out_file = args.out_file.split('.')[0] + '_CLICK.mid'

    generate_track_midi(module.TRACK, args.out_file, precount_sig=getattr(module, 'PRECOUNT_SIG', None))
    generate_track_click_midi(module.TRACK, click_out_file, precount_sig=getattr(module, 'PRECOUNT_SIG', None))
