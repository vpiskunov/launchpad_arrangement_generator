from lag import const


TRACK = [
    {
        'name': 'Intro 1',
        'coord': '1:1',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Intro 1.1',
        'coord': '1:2',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Intro 2',
        'coord': '1:3',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 1',
        'coord': '2:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 1.1',
        'coord': '2:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 1',
        'coord': '2:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2.1',
        'coord': '3:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 2',
        'coord': '3:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 1',
        'coord': '4:1',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (2, 4),
                'count': 1,
            },
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 2',
        'coord': '4:2',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 3',
        'coord': '4:3',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 4',
        'coord': '4:4',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 3',
        'coord': '5:1',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 3.1',
        'coord': '5:2',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    }
]
