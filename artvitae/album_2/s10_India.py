from lag import const


SOLO_PART = [
    {
        'sig': (5, 8),
        'count': 1,
    },
    {
        'sig': (7, 8),
        'count': 1,
    },
    {
        'sig': (5, 8),
        'count': 1,
    },
    {
        'sig': (11, 16),
        'count': 1,
    },
    {
        'sig': (5, 8),
        'count': 1,
    },
    {
        'sig': (7, 8),
        'count': 1,
    },
    {
        'sig': (5, 8),
        'count': 1,
    },
    {
        'sig': (6, 8),
        'count': 1,
    }
]

TRACK = [
    {
        'name': 'Intro 1',
        'coord': '2:1',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Intro 2',
        'coord': '2:3',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Intro 3',
        'coord': '2:4',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (3, 4),
                'count': 1,
            },
            # 1 Period
            {
                'sig': (4, 4),
                'count': 1,
            },
            {
                'sig': (7, 8),
                'count': 1,
            },
            {
                'sig': (4, 4),
                'count': 2,
            },
            # End period
            {
                'sig': (2, 4),
                'count': 1,
            },
        ]
    },

    {
        'name': 'Verse 1',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            # 1 Period
            {
                'sig': (4, 4),
                'count': 1,
            },
            {
                'sig': (7, 8),
                'count': 1,
            },
            {
                'sig': (4, 4),
                'count': 2,
            },
            # End period
            # 1 Period
            {
                'sig': (4, 4),
                'count': 1,
            },
            {
                'sig': (7, 8),
                'count': 1,
            },
            {
                'sig': (4, 4),
                'count': 2,
            },
            # End period
        ]
    },
    {
        'name': 'Pre chorus 1',
        'coord': '3:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 9,
            },
            {
                'sig': (2, 4),
                'count': 1,
            }
        ]
    },
    {
        'name': 'Play 1',
        'coord': '3:4',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },

    {
        'name': 'Verse 2',
        'coord': '4:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2.1',
        'coord': '4:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 2',
        'coord': '4:3',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 2.1',
        'coord': '4:4',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 6,
            }
        ]
    },

    # Strong
    {
        'name': 'Play 1',
        'coord': '5:1',
        'type': const.T_PLAY_HARD,
        'parts': SOLO_PART + SOLO_PART
    },
    {
        'name': 'Play 2',
        'coord': '5:2',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    # Solo
    {
        'name': 'Play 3',
        'coord': '5:4',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            }
        ]
    },
    {
        'name': 'Play 4',
        'coord': '5:6',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },

    # Final
    {
        'name': 'Pre chorus 2',
        'coord': '6:1',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 2.1',
        'coord': '6:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 6,
            }
        ]
    },

    {
        'name': 'Outro',
        'coord': '7:1',
        'type': const.T_PLAY_HARD,
        'parts': SOLO_PART
    },

]
