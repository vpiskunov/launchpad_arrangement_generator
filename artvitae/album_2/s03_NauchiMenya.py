from lag import const


TRACK = [
    {
        'name': 'Verse 1',
        'coord': '2:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 1',
        'coord': '2:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Chorus 1',
        'coord': '2:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 2',
        'coord': '3:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Chorus 2',
        'coord': '3:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre Chorus 3',
        'coord': '4:1',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 1',
        'coord': '5:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre Chorus 3.1',
        'coord': '5:3',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Chorus 3',
        'coord': '6:1',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 3.1',
        'coord': '6:2',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
]
