from lag import const

TRACK = [
    {
        'name': 'Intro 1',
        'coord': '2:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Intro 2',
        'coord': '2:2',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 7,
            },
            {
                'sig': (3, 4),
                'count': 1,
            }
        ]
    },
    {
        'name': 'Intro 3',
        'coord': '2:3',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 7,
            },
            {
                'sig': (5, 4),
                'count': 1,
            }
        ]
    },
    {
        'name': 'Verse 1',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            },
        ]
    },
    {
        'name': 'Pre Chorus 1',
        'coord': '3:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            },
        ]
    },
    {
        'name': 'Chorus 1',
        'coord': '3:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            },
        ]
    },
    {
        'name': 'Verse 2',
        'coord': '4:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            },
        ]
    },
    {
        'name': 'Pre Chorus 2',
        'coord': '4:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            },
        ]
    },
    {
        'name': 'Chorus 2',
        'coord': '4:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            },
        ]
    },
    {
        'name': 'Play 1',
        'coord': '5:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 18,
            },
        ]
    },
    {
        'name': 'Play 2',
        'coord': '5:2',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 2,
            },
        ]
    },
    {
        'name': 'Play 2.1',
        'coord': '5:4',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            },
        ]
    },
]
