from lag import const


PRECOUNT_SIG = (6, 4)

TRACK = [
    {
        'name': 'Intro 1',
        'coord': '2:1',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Intro 2',
        'coord': '2:2',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 1',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 1',
        'coord': '3:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (6, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Chorus 1',
        'coord': '3:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 1',
        'coord': '3:6',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2',
        'coord': '4:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 2',
        'coord': '4:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (6, 4),
                'count': 6,
            }
        ]
    },
    {
        'name': 'Chorus 2',
        'coord': '4:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 2',
        'coord': '4:6',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 3',
        'coord': '5:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 3',
        'coord': '5:2',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (6, 4),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Chorus 3',
        'coord': '5:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 3',
        'coord': '5:6',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 4',
        'coord': '6:1',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 4.1',
        'coord': '6:2',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 4',
        'coord': '7:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 5',
        'coord': '8:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 5.1',
        'coord': '8:2',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 5.2',
        'coord': '8:3',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (6, 4),
                'count': 8,
            }
        ]
    },
]
