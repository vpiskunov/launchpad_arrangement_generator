from lag import const

TRACK = [
    {
        'name': 'Intro 1',
        'coord': '2:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 10,
            }
        ]
    },
    {
        'name': 'Intro 2',
        'coord': '2:2',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },

    {
        'name': 'Verse 1',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 1.1',
        'coord': '3:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 1',
        'coord': '3:3',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 10,
            }
        ]
    },
    {
        'name': 'Chorus 1',
        'coord': '3:5',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            }
        ]
    },

    {
        'name': 'Pre verse 2',
        'coord': '3:7',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 4,
            }
        ]
    },

    {
        'name': 'Verse 2',
        'coord': '4:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2.1',
        'coord': '4:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Pre chorus 2',
        'coord': '4:3',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 10,
            }
        ]
    },
    {
        'name': 'Chorus 2',
        'coord': '4:5',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            }
        ]
    },

    {
        'name': 'Play 1',
        'coord': '5:1',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 2',
        'coord': '5:2',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 16,
            }
        ]
    },
    # Solo
    {
        'name': 'Play 2.1',
        'coord': '5:3',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 2.3',
        'coord': '5:4',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 10,
            }
        ]
    },

    {
        'name': 'Play 3',
        'coord': '5:6',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },

    {
        'name': 'Chorus 1',
        'coord': '6:1',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 24,
            }
        ]
    },

    {
        'name': 'Outro 1',
        'coord': '7:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (4, 4),
                'count': 3,
            }
        ]
    },
]
