from lag import const


TRACK = [
    {
        'name': 'Verse 1',
        'coord': '2:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 1.1',
        'coord': '2:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 1',
        'coord': '2:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 9,
            }
        ]
    },

    {
        'name': 'Verse 2',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2.1',
        'coord': '3:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 2',
        'coord': '3:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },

    {
        'name': 'Bridge 1',
        'coord': '4:1',
        'type': const.T_PRE_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 10,
            }
        ]
    },

    {
        'name': 'Chorus 3',
        'coord': '5:1',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 3.1',
        'coord': '5:2',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
]
