from lag import const

TRACK = [
    {
        'name': 'Intro 1',
        'coord': '2:1',
        'type': const.T_PLAY_LOW,
        'parts': [
            {
                'sig': (7, 8),
                'count': 12,
            }
        ]
    },
    {
        'name': 'Intro 1.1',
        'coord': '2:2',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (7, 8),
                'count': 4,
            }
        ]
    },
    {
        'name': 'Verse 1',
        'coord': '3:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (7, 8),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 1.1',
        'coord': '3:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (7, 8),
                'count': 7,
            },
            {
                'sig': (4, 4),
                'count': 1,
            }
        ]
    },
    {
        'name': 'Chorus 1',
        'coord': '3:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Play 1',
        'coord': '3:6',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (7, 8),
                'count': 4,
            }
        ]
    },

    {
        'name': 'Verse 2',
        'coord': '4:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (7, 8),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 2.1',
        'coord': '4:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (7, 8),
                'count': 7,
            },
            {
                'sig': (4, 4),
                'count': 1,
            }
        ]
    },
    {
        'name': 'Chorus 2',
        'coord': '4:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },

    {
        'name': 'Play 2',
        'coord': '5:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (9, 8),
                'count': 12,
            }
        ]
    },

    {
        'name': 'Verse 3',
        'coord': '6:1',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (7, 8),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Verse 3.1',
        'coord': '6:2',
        'type': const.T_VERSE,
        'parts': [
            {
                'sig': (7, 8),
                'count': 7,
            },
            {
                'sig': (4, 4),
                'count': 1,
            }
        ]
    },
    {
        'name': 'Chorus 3',
        'coord': '6:4',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },
    {
        'name': 'Chorus 3.1',
        'coord': '6:5',
        'type': const.T_CHORUS,
        'parts': [
            {
                'sig': (4, 4),
                'count': 8,
            }
        ]
    },

    {
        'name': 'Outro',
        'coord': '7:1',
        'type': const.T_PLAY_HARD,
        'parts': [
            {
                'sig': (9, 8),
                'count': 2,
            },
            {
                'sig': (4, 4),
                'count': 1,
            }
        ]
    },
]
