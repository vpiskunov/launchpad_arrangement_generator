# -*- coding: utf-8 -*-
import os


_ = os.path.dirname
BASE_DIR = _(os.path.abspath(__file__))


# Тиков на 1 четверть
PER_QUOTER = 24 * 4


# Типы частей песен
T_VERSE = 0
T_CHORUS = 1
T_PLAY_LOW = 2
T_PLAY_HARD = 3
T_PRE_CHORUS = 4
T_PRECOUNT = 4


# Маппинг цветов для частей песен
# Яркие цвета
# COLORS = {
#     T_VERSE: 79,
#     T_CHORUS: 5,
#     T_PLAY_LOW: 16,
#     T_PLAY_HARD: 22,
#     T_PRE_CHORUS: 53,
# }
# C_CURRENT = 3

# Блеклые цвета
COLORS = {
    T_VERSE: 80,
    T_CHORUS: 58,
    T_PLAY_LOW: 30,
    T_PLAY_HARD: 31,
    T_PRE_CHORUS: 78,
    T_PRECOUNT: 127,
}
C_CURRENT = 3


C_CLICK_TOGGLE = 9
C_CLICK_PAD = 127
C_PLAY = 63
C_STOP = 5
