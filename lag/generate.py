# -*- coding: utf-8 -*-
import midi

from lag import const


def gen_mapping():
    start_note = 36

    result = {}

    for i, n in enumerate(xrange(start_note, start_note + 64)):
        _row_number = i / 4
        _column_number = i % 4

        row_number = 8 - _row_number % 8
        column_number = _column_number + 1 + (_row_number / 8) * 4

        result['%s:%s' % (row_number, column_number)] = n

    return result


MAPPING = gen_mapping()
M_CLICK_TOGGLE = 36 + 64 + 8 + 1
M_CLICK_PAD = 36 + 64 - 1
M_PLAY = 36 + 64
M_STOP = 36 + 64 + 1


def generate_test_mapping_midi():
    """Генерация midi-трека, в результате работы
    которого должны по очереди зажечься все пады"""
    p = midi.Pattern(format=0, resolution=const.PER_QUOTER)
    t = midi.Track()
    p.append(t)

    t.append(
        midi.TimeSignatureEvent(tick=0, data=[4, 2, 36, 8])
    )

    # Добавляем ноты
    mapping = gen_mapping()
    for m in sorted(mapping):
        n = mapping[m]

        t.append(
            midi.NoteOnEvent(tick=0, velocity=20, pitch=n)
        )
        t.append(
            midi.NoteOffEvent(tick=const.PER_QUOTER / 8, pitch=n)
        )

    t.append(
        midi.EndOfTrackEvent(tick=1)
    )

    return p


def generate_track_midi(track, result_file=None, precount_sig=None):
    p = midi.Pattern(format=0, resolution=const.PER_QUOTER)
    t = midi.Track()

    p.append(t)

    # Настройки
    t.append(
        midi.TimeSignatureEvent(tick=0, data=[4, 2, 36, 8])
    )

    # Добавляем precount
    _track = [{
        'name': 'PRECOUNT',
        'coord': '1:1',
        'type': const.T_PRECOUNT,
        'parts': [
            {
                'sig': precount_sig or (4, 4),
                'count': 2,
            }
        ]
    }] + track

    all_notes = {}
    # Собираем все используемые ноты
    for part in _track:
        all_notes[part['coord']] = const.COLORS[part['type']]

    # Проходим по всем частям
    for part in _track:
        # Вычисляем длину части
        l = 0

        # Добавляем подсветку для сервисных кнопок
        t.append(midi.NoteOnEvent(tick=0, velocity=const.C_STOP, pitch=M_STOP))
        t.append(midi.NoteOnEvent(tick=0, velocity=const.C_PLAY, pitch=M_PLAY))
        t.append(midi.NoteOnEvent(tick=0, velocity=const.C_CLICK_TOGGLE, pitch=M_CLICK_TOGGLE))
        t.append(midi.NoteOnEvent(tick=0, velocity=const.C_CLICK_PAD, pitch=M_CLICK_PAD))

        for pp in part['parts']:
            num, dem = pp['sig']

            l += pp['count'] * num * const.PER_QUOTER * 4 / dem

        for a_note, a_note_color in all_notes.iteritems():
            if part['coord'] == a_note:
                # Данная часть активна
                color = const.C_CURRENT
            else:
                # Неактивна - оставляем как есть
                color = a_note_color

            t.append(
                midi.NoteOnEvent(tick=0, velocity=color, pitch=MAPPING[a_note])
            )

        for i, a_note in enumerate(all_notes.iterkeys()):
            if i == 0:
                t.append(
                    midi.NoteOffEvent(tick=l, pitch=MAPPING[a_note])
                )
            else:
                t.append(
                    midi.NoteOffEvent(tick=0, pitch=MAPPING[a_note])
                )

        # Завершение подсветки сервисных кнопок
        t.append(midi.NoteOffEvent(tick=0, pitch=M_STOP))
        t.append(midi.NoteOffEvent(tick=0, pitch=M_PLAY))
        t.append(midi.NoteOffEvent(tick=0, pitch=M_CLICK_TOGGLE))
        t.append(midi.NoteOffEvent(tick=0, pitch=M_CLICK_PAD))

    # Завершаем трек
    t.append(
        midi.EndOfTrackEvent(tick=1)
    )

    if result_file:
        midi.write_midifile(result_file, p)

    return p


def generate_track_click_midi(track, result_file=None, precount_sig=None):
    p = midi.Pattern(format=0, resolution=const.PER_QUOTER)
    t = midi.Track()

    p.append(t)

    # Настройки
    t.append(
        midi.TimeSignatureEvent(tick=0, data=[4, 2, 36, 8])
    )

    # Precount - 2 такта precount_sig
    if precount_sig:
        num, dem = precount_sig
    else:
        num, dem = (4, 4)

    # Проходим по тактам
    for bar in xrange(2):
        for tick in xrange(num):
            # Выделяем первую долю
            if tick == 0:
                note = midi.C_3
            else:
                note = midi.D_3

            t.append(
                midi.NoteOnEvent(tick=0, velocity=100, pitch=note)
            )
            t.append(
                midi.NoteOffEvent(tick=const.PER_QUOTER * 4 / dem, pitch=note)
            )

    # Проходим по всем частям
    for part in track:
        # Вычисляем длину части
        l = 0

        for pp in part['parts']:
            num, dem = pp['sig']

            # Проходим по тактам
            for bar in xrange(pp['count']):
                for tick in xrange(num):
                    # Выделяем первую долю
                    if tick == 0:
                        note = midi.C_3
                    else:
                        note = midi.D_3

                    t.append(
                        midi.NoteOnEvent(tick=0, velocity=100, pitch=note)
                    )
                    t.append(
                        midi.NoteOffEvent(tick=const.PER_QUOTER * 4 / dem, pitch=note)
                    )

    # Завершаем трек
    t.append(
        midi.EndOfTrackEvent(tick=1)
    )

    if result_file:
        midi.write_midifile(result_file, p)

    return p
